package my.id.airham.clienttrpla;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

public class BarangAdapter extends RecyclerView.Adapter<BarangAdapter.ViewHolder> {
    private final List<Barang> list;

    // context untuk Glide
    private final Context context;

    // Constructor
    public BarangAdapter(Context context, List<Barang> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public BarangAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_item_barang, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BarangAdapter.ViewHolder holder, int position) {
        Glide.with(context).load(list.get(position).getGambar()).into(holder.imgBarang);
        holder.txtNama.setText(list.get(position).getNamaBarang());
        holder.txtDetail.setText(list.get(position).getKeterangan());
        holder.txtHarga.setText("Harga: Rp." + list.get(position).getHarga());
        holder.txtPromo.setText("Promo: Rp." + list.get(position).getPromo());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imgBarang, star;
        TextView txtNama, txtDetail, txtHarga, txtPromo;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imgBarang = itemView.findViewById(R.id.imgBarang);
            star = itemView.findViewById(R.id.imgStar);
            txtNama = itemView.findViewById(R.id.txtNama);
            txtDetail = itemView.findViewById(R.id.txtDetail);
            txtHarga = itemView.findViewById(R.id.txtHarga);
            txtPromo = itemView.findViewById(R.id.txtPromo);
        }
    }
}
