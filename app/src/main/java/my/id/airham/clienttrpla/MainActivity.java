package my.id.airham.clienttrpla;

import static com.android.volley.Request.Method.GET;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    static final String URL_BARANG = "https://tokonya-andi.000webhostapp.com/show_barang.php";

    Context context;
    private RecyclerView myRecyclerView;
    private List<Barang> listBarang;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // binding untuk recyclerView
        myRecyclerView = findViewById(R.id.rcBarang);
        myRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        myRecyclerView.setHasFixedSize(true);
        listBarang = new ArrayList<>();
        DataBarang();
    }

    private void DataBarang() {
        // memanggil data dari network menggunakan HTTP header GET
        StringRequest request = new StringRequest(GET, URL_BARANG,
                // jika berhasil mendapatkan data (HTTP.statusCode == 200)
                (Response.Listener<String>) response -> {
                    try {
                        JSONArray array = new JSONArray(response);
                        for (int i = 0; i < array.length(); i++) {
                            Barang barang = new Barang();
                            JSONObject object = array.getJSONObject(i);
                            barang.setId(object.getInt("id"));
                            barang.setNamaBarang(object.getString("nama"));
                            barang.setKeterangan(object.getString("keterangan"));
                            barang.setHarga(String.valueOf(object.getDouble("harga")));
                            barang.setGambar(object.getString("gambar"));
                            barang.setPromo(String.valueOf(object.getDouble("promo")));
                            listBarang.add(barang);
                        }

                        BarangAdapter adapter = new BarangAdapter(MainActivity.this, listBarang);
                        myRecyclerView.setAdapter(adapter);
                        Log.d("TAG", "DataBarang: " + response);

                    } catch (JSONException e) {
                        // menangkap error jika terdapat kesalahan parsing JSON
                        e.printStackTrace();
                    }
                }, error -> Log.d("TAG", error.getMessage()));

        // buat antrian menggunakan method newRequestQueue
        Volley.newRequestQueue(this).add(request);
    }
}